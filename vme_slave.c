/* Tool for configuring VME Slave Windows.
 *
 * Copyright 2015 Martyn Welch <martyn@welchs.me.uk>
 * Copyright 2016 Martyn Welch <martyn.welch@collabora.co.uk>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, version 2 of 
 * the License.
 *
 */
#define _XOPEN_SOURCE 600
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "config.h"
#include "vme_user.h"

/* VME Address Spaces */
#define VME_A16         0x1
#define VME_A24         0x2
#define VME_A32         0x4
#define VME_A64         0x8

#define VME_SUPER       0x1000
#define VME_USER        0x2000
#define VME_PROG        0x4000
#define VME_DATA        0x8000

void usage(void)
{
	printf("%s: VME slave utility\n\n", PACKAGE_NAME);
	printf("Usage: vme_slave [OPTION] FILE\n\n");
	printf("Get and set configuration of VME slave window FILE\n\n");
	printf(" -a ADDRESS  set window base address to ADDRESS\n");
	printf(" -D          set window to respond to data accesses (default, can not be used\n");
	printf("             with \"-P\")\n");
	printf(" -e ENABLE   enable or disable window (0-disable, 1-enable)\n");
	printf(" -h          display this help and exit\n");
	printf(" -l LENGTH   set window size to LENGTH\n");
	printf(" -N          set window to respond to non-privileged accesses (default, can not\n");
	printf("             be used with \"-S\")\n");
	printf(" -p          print VME window configuration\n");
	printf(" -P          set window to respond to program accesses (can not be used with\n");
	printf("             \"-D\")\n");
	printf(" -s SPACE    set window address space to SPACE (accepts 16, 24, 32 or 64)\n");
	printf(" -S          set window to respond to supervisory accesses (can not be used\n");
	printf("             with \"-N\")\n");
	printf(" -v          display version information and exit\n\n");
}

void version(void)
{
	printf("%s\n", PACKAGE_STRING);
}

void show_settings(char *file, struct vme_slave *slave)
{
	int space;
	int width;
	char *mode;
	char *type;


	switch (slave->aspace) {
	case VME_A16:
		space = 16;
		break;
	case VME_A24:
		space = 24;
		break;
	case VME_A32:
		space = 32;
		break;
	case VME_A64:
		space = 64;
		break;
	}

	if ((slave->cycle & VME_SUPER) != 0)
		mode = "Supervisory";
	else
		mode = "Non-privileged";

	if ((slave->cycle & VME_PROG) != 0)
		type = "program";
	else
		type = "data";

	printf("Slave Window: %s:\n", file);
	printf("  Enabled:       %s\n", (slave->enable == 1) ? "Yes" : "No");
	printf("  Address:       0x%x\n", slave->vme_addr);
	printf("  Size:          0x%x\n", slave->size);
	printf("  Address space: A%d\n", space);
	printf("  Access mode:   %s %s access\n", mode, type);
}

int main(int argc, char *argv[])
{
        int fd;
        int i;
	int opt;
        int retval;
	int val;
	char *end;
	bool show_set = false;
	bool set_enable = false;
	int enable;
	bool set_address = false;
	uint64_t address;
	bool set_length = false;
	uint64_t length;
	bool set_aspace = false;
	int aspace;
	bool set_access = false;
	bool nonpriv = false;
	bool super = false;
	bool data = false;
	bool prog = false;

        struct vme_slave slave;

	while ((opt = getopt(argc, argv, "a:De:hl:NpPs:Sv")) != -1) {
		switch (opt) {
		case 'a':
			set_address = true;
			errno = 0;
			address = strtoll(optarg, &end, 0);
			if ((errno == ERANGE
			     && (address == LLONG_MAX || address == LLONG_MIN))
			    || (errno != 0 && address == 0)) {
				perror("Window base address");
				return EX_USAGE;
			}
			if (optarg == end) {
				fprintf(stderr, "Window base address: Argument should be numeric\n");
				return EX_USAGE;
			}
			break;
		case 'D':
			set_access = true;
			data = true;
			if (prog == true) {
				fprintf(stderr, "Can not set data and program accesses simultaneously\n");
				return EX_USAGE;
			}
			break;
		case 'e':
			set_enable = true;
			if (strcmp(optarg, "1") == 0) {
				enable = 1;
			} else {
				enable = 0;
			}
			break;
		case 'l':
			set_length = true;
			errno = 0;
			length = strtoll(optarg, &end, 0);
			if ((errno == ERANGE
			     && (length == LLONG_MAX || length == LLONG_MIN))
			    || (errno != 0 && length == 0)) {
				perror("Window size");
				return EX_USAGE;
			}
			if (optarg == end) {
				fprintf(stderr, "Window size: Argument should be numeric\n");
				return EX_USAGE;
			}
			break;
		case 'N':
			set_access = true;
			nonpriv = true;
			if (super == true) {
				fprintf(stderr, "Can not set supervisory and non-privileged accesses simultaneously\n");
				return EX_USAGE;
			}
			break;
		case 'P':
			set_access = true;
			prog = true;
			if (data == true) {
				fprintf(stderr, "Can not set data and program accesses simultaneously\n");
				return EX_USAGE;
			}
			break;
		case 'p':
			show_set = true;
			break;
		case 'S':
			set_access = true;
			super = true;
			if (nonpriv == true) {
				fprintf(stderr, "Can not set supervisory and non-privileged accesses simultaneously\n");
				return EX_USAGE;
			}
			break;
		case 's':
			set_aspace = true;
			val = atoi(optarg);
			switch (val) {
			case 16:
				aspace = VME_A16;
				break;
			case 24:
				aspace = VME_A24;
				break;
			case 32:
				aspace = VME_A32;
				break;
			case 64:
				aspace = VME_A64;
				break;
			default:
				fprintf(stderr, "Address space: Invalid address space\n");
				return EX_USAGE;
			}
			break;
		case 'v':
			version();
			return EX_OK;
		case 'h':
		default:
			usage();
			return EX_OK;
		}
	}

	if (optind >= argc) {
		usage();
		return EX_USAGE;
	}

	if (optind == 1) {
		/* No options provided - set default */
		show_set = true;
	}

        fd = open(argv[optind], O_RDONLY);
        if (fd == -1) {
                perror("ERROR: Opening window device file");
                return EX_OSFILE;
        }

        retval = ioctl(fd, VME_GET_SLAVE, &slave);
        if (retval != 0) {
                perror("ERROR: Failed to configure window");
                retval = EX_IOERR;
		goto err1;
        }

	if (set_enable)
		slave.enable = enable;

	if (set_address)
		slave.vme_addr = address;

	if (set_length)
		slave.size = length;

	if (set_aspace)
		slave.aspace = aspace;

	if (set_access) {
		if (super) {
			slave.cycle &= ~VME_USER;
			slave.cycle |= VME_SUPER;
		} else {
			slave.cycle &= ~VME_SUPER;
			slave.cycle |= VME_USER;
		}

		if (prog) {
			slave.cycle &= ~VME_DATA;
			slave.cycle |= VME_PROG;
		} else {
			slave.cycle &= ~VME_PROG;
			slave.cycle |= VME_DATA;
		}
	}

	if (show_set)
		show_settings(argv[optind], &slave);

	if (set_enable || set_address || set_length || set_aspace || set_access) {
		retval = ioctl(fd, VME_SET_SLAVE, &slave);
		if (retval != 0) {
			perror("ERROR: Failed to configure window");
			return 1;
		}
	}
err1:
        close(fd);

        return retval;
}
